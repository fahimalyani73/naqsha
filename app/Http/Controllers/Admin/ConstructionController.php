<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Construction;

class ConstructionController extends Controller
{
	public function index(){
    	$cc = Construction::all();
    	// dd($cc);
    	return view('admin/construction/construction',compact('cc'));
    }

    public function add_town_planning(){
    	return view('admin/construction/add-construction');
    }
    public function store(Request $request){
    	$townp = new Construction();
    	$townp->subcribed_company = $request->subcribed_company;
    	$townp->plot_location = $request->plot_location;
    	$townp->package = $request->package;
    	$townp->plot_size = $request->plot_size;
    	$townp->covered_area = $request->covered_area;
    	$townp->type = $request->type;
    	$townp->rate_sqft = $request->rate_sqft;
    	$townp->total = $request->total;
    	$townp->details = $request->details;
    	$townp->get_quotation = $request->get_quotation;
    	
    	$townp->save();
    	return back();	
    }
    public function edit($id){
    	$tp = Construction::find($id);
    	return view('admin/construction/add-construction',compact('tp'));
    }
    public function update(Request $request,$id){
    	$townp = Construction::find($id);
    	$townp->subcribed_company = $request->subcribed_company;
    	$townp->plot_location = $request->plot_location;
    	$townp->package = $request->package;
    	$townp->plot_size = $request->plot_size;
    	$townp->covered_area = $request->covered_area;
    	$townp->type = $request->type;
    	$townp->rate_sqft = $request->rate_sqft;
    	$townp->total = $request->total;
    	$townp->details = $request->details;
    	$townp->get_quotation = $request->get_quotation;
    	$townp->save();
    	return back();	
    }
    public function destroy($id){
    	Construction::find($id)->delete();
    	return back();
    }
    //
}
