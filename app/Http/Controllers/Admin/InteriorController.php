<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Intereior;
class InteriorController extends Controller
{
    public function index() {
    	$cc = Intereior::all();

    	return view('admin/interior/interior',compact('cc'));
    }
    public function add_interior(){
    	return view('admin/interior/add_interior');
    }
    public function store(Request $request){
    	$townp = new Intereior();
    	$townp->subcribed_company = $request->subcribed_company;
    	$townp->plot_location = $request->plot_location;
    	// $townp->package = $request->package;
    	// $townp->plot_size = $request->plot_size;
    	$townp->covered_area = $request->covered_area;
    	// $townp->type = $request->type;
    	$townp->rate_sqft = $request->rate_sqft;
    	$townp->total = $request->total;
    	$townp->details = $request->details;
    	$townp->get_quotation = $request->get_quotation;
    	
    	$townp->save();
    	return back();	
    }
    public function edit($id){
    	$tp = Intereior::find($id);
    	return view('admin/interior/add_interior',compact('tp'));
    }
    public function update(Request $request,$id){
    	$townp = Intereior::find($id);
    	$townp->subcribed_company = $request->subcribed_company;
    	$townp->plot_location = $request->plot_location;
    	// $townp->package = $request->package;
    	// $townp->plot_size = $request->plot_size;
    	$townp->covered_area = $request->covered_area;
    	// $townp->type = $request->type;
    	$townp->rate_sqft = $request->rate_sqft;
    	$townp->total = $request->total;
    	$townp->details = $request->details;
    	$townp->get_quotation = $request->get_quotation;
    	$townp->save();
    	return back();	
    }
    public function destroy($id){
    	Intereior::find($id)->delete();
    	return back();
    }
}
