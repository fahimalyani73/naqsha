<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TownPlanning;

class TownPlaningController extends Controller
{
    public function index(){
    	$tp = TownPlanning::all();
    	return view('admin/townplanning/twon_planing',compact('tp'));
    }

    public function add_town_planning(){
    	return view('admin/townplanning/add-town-planning');
    }
    public function store(Request $request){
    	$townp = new TownPlanning();
    	$townp->subcribed_company = $request->subcribed_company;
    	$townp->town_location = $request->town_location;
    	$townp->covered_area = $request->covered_area;
    	$townp->rate_sqft = $request->rate_sqft;
    	$townp->total = $request->total;
    	$townp->details = $request->details;
    	$townp->get_quotation = $request->get_quotation;
    	
    	$townp->save();
    	return back();	
    }
    public function edit($id){
    	$tp = TownPlanning::find($id);
    	return view('admin/townplanning/add-town-planning',compact('tp'));
    }
    public function update(Request $request,$id){
    	$townp = TownPlanning::find($id);
    	$townp->subcribed_company = $request->subcribed_company;
    	$townp->town_location = $request->town_location;
    	$townp->covered_area = $request->covered_area;
    	$townp->rate_sqft = $request->rate_sqft;
    	$townp->total = $request->total;
    	$townp->details = $request->details;
    	$townp->get_quotation = $request->get_quotation;
    	$townp->save();
    	return back();	
    }
    public function destroy($id){
    	TownPlanning::find($id)->delete();
    	return back();
    }
}
