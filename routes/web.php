<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','usercontroller@index');
Route::get('admin','Admin\AdminController@index');
Route::get('town-planing','Admin\TownPlaningController@index');

Route::get('add-town-planing','Admin\TownPlaningController@add_town_planning');
Route::post('store-town-planing','Admin\TownPlaningController@store');
Route::get('edit-town-planing/{id}','Admin\TownPlaningController@edit');
Route::post('update-town-planing/{id}','Admin\TownPlaningController@update');
Route::post('tp-destroy/{id}','Admin\TownPlaningController@destroy');
					//construction//
Route::get('construction','Admin\ConstructionController@index');
Route::get('add-construction','Admin\ConstructionController@add_town_planning');
Route::post('store-construction','Admin\ConstructionController@store');
Route::get('edit-construction/{id}','Admin\ConstructionController@edit');
Route::post('update-construction/{id}','Admin\ConstructionController@update');
Route::post('cc-destroy/{id}','Admin\ConstructionController@destroy');
					// interior//
Route::get('interior','Admin\InteriorController@index');
Route::get('add_interior','Admin\InteriorController@add_interior');
Route::post('store-interior','Admin\InteriorController@store');
Route::get('edit-interior/{id}','Admin\InteriorController@edit');
Route::post('update-interior/{id}','Admin\InteriorController@update');
Route::post('cc-destroy/{id}','Admin\InteriorController@destroy');

					//website folder routh//
 Route::get('interiors','Website\InteriorController@index')->name('filter.interiors');
  Route::get('constructions','Website\ConstructionsController@index')->name('filter.constructions');
   Route::get('townplaning','Website\TownPlaningController@index')->name('filter.townplaning');