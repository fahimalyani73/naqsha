<style type="text/css">
    #buttons li {
  float: left;
    list-style: none;
    text-align: center;
    background-color: #000000;
    margin-right: 3px;
    width: 105px;
    line-height: 46px;
}
.company-count li{
    list-style: none;   
}
.company-count li a{
    text-decoration: none;
    color: #FFFFFF;
    display: block;   
}
.arch_count_id{
    text-align: right;
}

#buttons li a {
  text-decoration: none;
  color: #FFFFFF;
  display: block;
}

#buttons li a:hover {
  text-decoration: none;
  color: #000000;
  background-color: #33B5E5;
}
.text-register-comp{
    text-align: center;
}
</style>
<div class="col-md-12 col-xl-12 col-lg-12 col-xs-12 col-sm-12">
    <div class="col-sm-8">
        <div class="text-center">
            <img id="naqsha_pvt_ltd" class=" img-responsive d-lg-block d-md-none d-none" src="https://www.naqsha.com.pk/wp-content/themes/naqsha/img/logo-5.png?v1.2" alt="logo" style="height:50px;">
            <h4 class="" style="text-align: center;">Select service below to Get Quotation</h4>
            <div class="banner-info">
                <ul id="buttons">
                    <li><a href="#">Architecture</a></li>
                    <li><a href="#">Construction</a></li>
                    <li><a href="#">Interior</a></li>
                    <li><a href="#">Landscape</a></li>
                    <li><a href="#">Town Planning</a></li>
                    <li><a href="#">Engineering</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="row d-none d-xl-block">
            <div class="jumbotron aos-init aos-animate" data-aos="fade-left" data-aos-duration="3000">
                <h3 class="pb-3 text-register-comp">Registered Companies</h3>
                <ul class="company-count" style="">

                    <a href="">
                        <li class="pb-2"> Architecture <span class="arch_count_id">100 listings</span></li>
                    </a>
                    <a href="">
                        <li class="pb-2"> Construction <span class="con_count_id">176 listings</span></li>
                    </a>
                    <a href="">
                        <li class="pb-2"> Interior <span class="inter_count_id">61 listings</span></li>
                    </a>
                    <a href="">
                        <li class="pb-2"> Landscape <span class="land_count_id">15 listings</span></li>
                    </a>
                </ul>
            </div>
        </div>
    </div>
</div>