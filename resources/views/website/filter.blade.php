<style type="text/css">
   .filter-inputs{
      background: transparent;
      color: #fff;
      border-radius: 50px;
   }
</style>
<div class="container">
   <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-pad filter-background-color">
         <!-- Architecture Form -->
         <form method="post" id="architecture_form">
            <div class="submit-address dashboard-list">
             
               <div class="row">
                  <h3 class="text-center" style="color: #fff;">Construction</h3>
                  <h5 style="padding-left: 30px;
                     color: white;">
                     Select the options given below 
                  </h5>
               </div>
               <div class="contact-2">
                  <div class="row pad-2">
                     <div class="col-md-4 col-sm-12">
                        <div class="form-group css_custom_select">
                           <label>Professional Location</label>
                           <select class="form-control filter-inputs" name="professional_location" id="professional_location" tabindex="-1" aria-hidden="true">
                              <option value="1470-1">Jinnah Colony, Faisalabad</option>
                           </select>
                        </div>
                        <div class="form-group css_custom_select">
                           <label>Plot Location </label>
                           <select class="form-control filter-inputs" name="plot_location" id="plot_location" >
                              <option value="1470-1">Jinnah Colony, Faisalabad</option>
                           </select>
                           
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-12">
                        <div class="form-group">
                           <label>Type</label>
                           <select class="form-control text-light bg-dark filter-inputs" id="project_type" name="porperty">
                              <option value="arc_res_">Residential</option>
                              <option value="arc_comm_">Commercial</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label>Plot Size</label>
                           <select class="form-control text-light bg-dark filter-inputs"  id="plot_size" name="size">
                              undefined
                              <option value="5m">5 Marla</option>
                              <option value="10m">10 Marla</option>
                              <option value="1k">1 kanal</option>
                              <option value="2k">2 Kanal</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-12">
                        <div class="form-group" id="floor_main_div">
                           <label>No. of Floors</label>
                           <select class="form-control text-light bg-dark filter-inputs" name="construction_type" id="construction_type">
                              <option value="B">2 Floor + Basement</option>
                           </select>
                        </div>
                        <div class="form-group mb-0" style="margin-top:50px;">
                           <button id="btnSearch" class="btn btn-primary" type="button" style="width:100%;">Search</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </form>
         <style>
            .select2 {
            width: 100% !important;
            }
         </style>
      </div>
   </div>
</div>