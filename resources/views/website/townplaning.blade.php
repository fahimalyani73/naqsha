@extends('website/master-layout')
@push('css')

@endpush
@section('content')
<div class="row">

    <div class="col-sm-8 col-sm-offset-2">
        <table id="constructions" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>subcribed_company</th>
                    <th>town_location</th>
                    <th>covered_area</th>
                    <th>rate_sqft</th>
                    <th>total</th>
                    <th>details</th>
                     <th>get_quotation</th>
                     
                </tr>
            </thead>
            <tbody> 
                @foreach($cc as $key => $value)
                <tr>
                    <td scope="row">{{++$key}}</td>
                    <td>{{$value->subcribed_company}}</td>
                    <td>{{$value->town_location}}</td>
                    <td>{{$value->covered_area}}</td>
                    <td>{{$value->rate_sqft}}</td>
                    <td>{{$value->total}}</td>
                    <td>{{$value->details}}</td>
                    <td>{{$value->get_quotation}}</td>
              

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


@endsection

@push('scripts')


<script type="text/javascript">
    $(document).ready(function() {
    $('#constructions').DataTable();
});
</script>
@endpush
