<nav class="navbar navbar-inverse">
   <div class="container-fluid">
      <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>                        
         </button>
         <a class="navbar-brand" href="#">WebSiteName</a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
         <ul class="nav navbar-nav navbar-right">
            <li class="nav-item dropdown">
               <a class="nav-link" href="#" data-toggle="modal" data-target="#get_quotation" aria-haspopup="true" aria-expanded="false">
               How to Get Quotation
               </a>
            </li>
            <li class="nav-item dropdown active">
               <a class="nav-link dropdown-toggle" href="https://www.naqsha.com.pk/architecture" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               Services 
               </a>
               <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <li><a class="dropdown-item" href="">Architecture</a></li>
                  <li><a class="dropdown-item" href="">Construction</a></li>
                  <li><a class="dropdown-item" href="">Interior</a></li>
                  <li><a class="dropdown-item" href="">Landscape</a></li>
                  <li><a class="dropdown-item" href="">Town Planning</a></li>
                  <li><a class="dropdown-item" href="">Engineering</a></li>
               </ul>
            </li>
            <li class="nav-item dropdown">
               <a class="nav-link" href="https://www.naqsha.com.pk/about-us">
               About us
               </a>
            </li>
            <li class="nav-item dropdown">
               <a class="nav-link" href="https://www.naqsha.com.pk/terms">
               Terms &amp; Conditions
               </a>
            </li>
            <li class="nav-item dropdown">
               <a class="nav-link" href="https://www.naqsha.com.pk/faq">
               FAQs
               </a>
            </li>
            <li class="nav-item dropdown">
               <a class="nav-link" href="https://www.naqsha.com.pk/blogs">
               Blogs    
               </a>                                           
            </li>
            <li class="nav-item dropdown">
               <a class="nav-link" id="btn-contact-us">
               Contact Us
               </a>
            </li>
         </ul>
      </div>
   </div>
</nav>