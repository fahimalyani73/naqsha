@extends('website/master-layout')
@push('css')
<style type="text/css">
    /* carousel */
.media-carousel 
{
  margin-bottom: 0;
  padding: 0 40px 30px 40px;
  margin-top: 30px;
}
/* Previous button  */
.media-carousel .carousel-control.left 
{
  left: -12px;
  background-image: none;
  background: none repeat scroll 0 0 #222222;
  border: 4px solid #FFFFFF;
  border-radius: 23px 23px 23px 23px;
  height: 40px;
  width : 40px;
  margin-top: 30px
}
/* Next button  */
.media-carousel .carousel-control.right 
{
  right: -12px !important;
  background-image: none;
  background: none repeat scroll 0 0 #222222;
  border: 4px solid #FFFFFF;
  border-radius: 23px 23px 23px 23px;
  height: 40px;
  width : 40px;
  margin-top: 30px
}
/* Changes the position of the indicators */
.media-carousel .carousel-indicators 
{
  right: 50%;
  top: auto;
  bottom: 0px;
  margin-right: -19px;
}
/* Changes the colour of the indicators */
.media-carousel .carousel-indicators li 
{
  background: #c0c0c0;
}
.media-carousel .carousel-indicators .active 
{
  background: #333333;
}
.media-carousel img
{
  width: 250px;
  height: 100px
}
/* End carousel */
*{
   font-family: 'Poppins', sans-serif;
}
.testimonial_subtitle{
    color: #0aaa7a;
    font-size: 12px;
}
  .testimonial_btn{
    background-color: #373d4b !important;
    color: #fff !important;
 }
 .seprator {
    height: 2px;
    width: 56px;
    background-color: #0aaa7a;
    margin: 7px 0 10px 0;
}
.buttons{
  padding: 20px 30px !important;
  border-radius: 0px;
  background: #000;
  border-color: #000;
}
.ul-buttons{
  text-align: center;
  margin-top: 400px;
}
</style>
@endpush
@section('content')

        <!-- Banner Section Start -->
       
        <!-- Banner Section End -->

        <!-- Appointment Section Start -->
       <!--  <div id="appointment" class="appointment--section pd--100-0-40">
            <div class="container">
                <div class="row row--vc">
                  
                    <!-- Appointment Form End -->
       


                    <div class="appointment--items col-md-8">
                        <div class="row AdjustRow">
                            <div class="col-xs-6 col-xxs-12">
                                <!-- Appointment Item Start -->
                               
                                <!-- Appointment Item End -->
                            </div>

                           

                            <div class="col-xs-6 col-xxs-12">
                                <!-- Appointment Item Start -->
                               <!--  <div class="appointment--item">
                                    <span class="step font--secondary">Step - 03</span>

                                    <div class="icon text--primary">
                                        <img src="img/appointment-img/icon-03.png" alt="" data-rjs="2">
                                    </div>

                                    <div class="title">
                                        <h2 class="h4">Make An Appoinment</h2>
                                    </div>

                                    <div class="content">
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                    </div>
                                </div> -->
                                <!-- Appointment Item End -->
                            </div>

                           
                        </div>
                    </div>
                    <!-- Appointment Items End -->
                </div>
            </div>
        </div> 
        <!-- sponser add -->
<div class="container">
  <div class="row">
    <div class="col-md-12"><h2 class="text-center">Sponser Ad</h2></div>
    
  </div>
  <div class='row'>
    <div class='col-md-12'>
      <div class="carousel slide media-carousel" id="media">
        <div class="carousel-inner">
          <div class="item  active">
            <div class="row">
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>          
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>        
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>          
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>        
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>          
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>      
            </div>
          </div>
        </div>
        <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
        <a data-slide="next" href="#media" class="right carousel-control">›</a>
      </div>                          
    </div>
  </div>
</div>
        <!-- Appointment Section End -->
        <div class="blog--section pd--100-0-70">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title">
                    <h2 class="text-black">How can you get professionals from our website</h2>

                    <p>Get a quotation in three simple steps</p>
                </div>
                <!-- Section Title End -->

                <div class="row">
                    <div class="col-md-4 col-xs-6 col-xxs-12">
                        <!-- Post Item Start -->
                        <div class="post--item">
                            <div class="post--img">
                                <img src="{{asset('public/assets')}}/img/blog-img/post-item-01.jpg" alt="" data-rjs="2">
                            </div>

                            <div class="post--info">
                                <h3 class="h5"><a href="blog-details.html">Simple Search</a></h3>

                               
                            </div>

                            <div class="post--content">
                                <p>Search using appropriate filters.</p>
                            </div>

                           
                        </div>
                        <!-- Post Item End -->
                    </div>

                    <div class="col-md-4 col-xs-6 col-xxs-12">
                        <!-- Post Item Start -->
                        <div class="post--item">
                            <div class="post--img">
                                <img src="{{asset('public/assets')}}/img/blog-img/post-item-02.jpg" alt="" data-rjs="2">
                            </div>

                            <div class="post--info">
                                <h3 class="h5"><a href="blog-details.html">Check Portfolios and Pricing</a></h3>

                               
                            </div>

                            <div class="post--content">
                                <p>View company portfolios and select your builder/designer..</p>
                            </div>

                           
                        </div>
                        <!-- Post Item End -->
                    </div>

                    <div class="col-md-4 hidden-sm hidden-xs show-xxs">
                        <!-- Post Item Start -->
                        <div class="post--item">
                            <div class="post--img">
                                <img src="{{asset('public/assets')}}/img/blog-img/post-item-03.jpg" alt="" data-rjs="2">
                            </div>

                            <div class="post--info">
                                <h3 class="h5"><a href="blog-details.html">Get Quotation/ Contact Professional</a></h3>

                              
                            </div>

                            <div class="post--content">
                                <p>Select your best quotation from result.</p>
                            </div>

                            
                        </div>
                        <!-- Post Item End -->
                    </div>
                </div>
            </div>
        </div>

<section>
     <div class="experts--section pd--100-0">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title">
                    <h2 class="h2">Premium Plus Companies</h2>

                    <p>Featured architects, builders, & designers at a glance</p>
                </div>
                <!-- Section Title End -->

                <!-- Expert Members Start -->
                <div class="expert--members owl-carousel" data-owl-items="4" data-owl-margin="30" data-owl-responsive='{"0": {"items": "1"}, "551": {"items": "2"}, "992": {"items": "3"}, "1200": {"items": "4"}}'>
                    <!-- Expert Member Start -->
                    <div class="expert--member">
                        <div class="expert--member-img">
                            <img src="{{asset('public/assets')}}/img/experts-img/member-01.jpg" alt="" data-rjs="2">

                            <div class="expert--member-info bg--overlay">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <div class="name">
                                            <h3 class="h5">abcd</h3>
                                        </div>

                                        <div class="role">
                                            <p>Dumy Data</p>
                                        </div>

                                        <div class="content">
                                            <p>Hello! I am Sahjahan Saju. I am Computer Repair Expert.</p>
                                        </div>

                                        <div class="social">
                                            <ul class="nav">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Expert Member End -->

                 
                    <!-- Expert Member End -->

                    <!-- Expert Member Start -->
                    <div class="expert--member">
                        <div class="expert--member-img">
                            <img src="{{asset('public/assets')}}/img/experts-img/member-03.jpg" alt="" data-rjs="2">

                            <div class="expert--member-info bg--overlay">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <div class="name">
                                            <h3 class="h5">Zobayer Hossen Manik</h3>
                                        </div>

                                        <div class="role">
                                            <p>iPad/Tablet Repair Expert</p>
                                        </div>

                                        <div class="content">
                                            <p>Hello! I am Zobayer Hossen Manik. I am iPad/Tablet Repair Expert.</p>
                                        </div>

                                        <div class="social">
                                            <ul class="nav">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Expert Member End -->

                    <!-- Expert Member Start -->
                    <div class="expert--member">
                        <div class="expert--member-img">
                            <img src="{{asset('public/assets')}}/img/experts-img/member-04.jpg" alt="" data-rjs="2">

                            <div class="expert--member-info bg--overlay">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <div class="name">
                                            <h3 class="h5">Sabbir Ahmed</h3>
                                        </div>

                                        <div class="role">
                                            <p>Mobile Repair Expert</p>
                                        </div>

                                        <div class="content">
                                            <p>Hello! I am Sabbir Ahmed. I am Mobile Repair Expert.</p>
                                        </div>

                                        <div class="social">
                                            <ul class="nav">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Expert Member End -->

                    <!-- Expert Member Start -->
                    <div class="expert--member">
                        <div class="expert--member-img">
                            <img src="{{asset('public/assets')}}/img/experts-img/member-01.jpg" alt="" data-rjs="2">

                            <div class="expert--member-info bg--overlay">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <div class="name">
                                            <h3 class="h5">Sahjahan Saju</h3>
                                        </div>

                                        <div class="role">
                                            <p>Computer Repair Expert</p>
                                        </div>

                                        <div class="content">
                                            <p>Hello! I am Sahjahan Saju. I am Computer Repair Expert.</p>
                                        </div>

                                        <div class="social">
                                            <ul class="nav">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Expert Member End -->

                    <!-- Expert Member Start -->
                    <div class="expert--member">
                        <div class="expert--member-img">
                            <img src="{{asset('public/assets')}}/img/experts-img/member-02.jpg" alt="" data-rjs="2">

                            <div class="expert--member-info bg--overlay">
                                <div class="vc--parent">
                                    <div class="vc--child">
                                        <div class="name">
                                            <h3 class="h5">Mahmudullah Ripon</h3>
                                        </div>

                                        <div class="role">
                                            <p>Laptop Repair Expert</p>
                                        </div>

                                        <div class="content">
                                            <p>Hello! I am Mahmudullah Ripon. I am Laptop Repair Expert.</p>
                                        </div>

                                        <div class="social">
                                            <ul class="nav">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Expert Member End -->
                </div>
                <!-- Expert Members End -->

                <!-- Expert Members Nav Start -->
                <div class="expert--members-nav text-center">
                    <div class="btn-groups">
                        <button type="button" data-trigger="prev.owl.carousel"><i class="fa fm fa-long-arrow-left"></i>Previous</button>

                        <a href="experts.html" title="View All Experts" data-toggle="tooltip" data-placement="top"><i class="fa fa-th-large"></i></a>

                        <button type="button" data-trigger="next.owl.carousel">Next<i class="fa flm fa-long-arrow-right"></i></button>
                    </div>
                </div>
                <!-- Expert Members Nav End -->
            </div>
        </div>
</section>
        <!-- Services Section Start -->
       
        <!-- Services Section End -->

        <!-- Features Section Start -->
        <div class="features--section pd--100-0-40">
            <div class="container"style="padding-left: 20px;">
              <!-- Section Title Start -->
                <div class="section--title">
                    <h2 class="h2">Gallery</h2>
                </div>
                <!-- Section Title End -->

               
            </div>
        </div>
        <!-- Features Section End -->

        <!-- Counter Section Start -->
        <div class="counter--section pd--100-0-40" style="margin-top:-198px;">
             <div class="row">
                <img src="{{asset('public/assets')}}/img/b1.jpeg" alt="" data-rjs="2" style ="width:1363px;padding-left:50px;padding-right:50px;">
             </div>
            <div class="container">
                <div class="row">
  <div class="column" >
    <img src="{{asset('public/assets')}}/img/b1.jpeg" alt="" data-rjs="2">
  </div>
  <div class="column" >
   <img src="{{asset('public/assets')}}/img/b1.jpeg" alt="" data-rjs="2">
  </div>
</div>
                
      
                   
                    
                </div>
            </div>
       
       <section>
    
</section> 

        <!-- Blog Section Start -->
        <div class="blog--section pd--100-0-70">
            <div class="container">
                <!-- Section Title Start -->
                <div class="section--title">
                    <h2 class="h2">Premium Companies</h2>

                    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="container">
  <div class="row">
    <div class="col-md-12"><h2 class="text-center"></h2></div>
    
  </div>
  <div class='row'>
    <div class='col-md-12'>
      <div class="carousel slide media-carousel" id="media">
        <div class="carousel-inner">
          <div class="item  active">
            <div class="row">
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>          
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>        
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>          
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>        
            </div>
          </div>
          <div class="item">
            <div class="row">
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>          
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>
              <div class="col-md-4">
                <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
              </div>      
            </div>
          </div>
        </div>
        <a data-slide="prev" href="#media" class="left carousel-control">‹</a>
        <a data-slide="next" href="#media" class="right carousel-control">›</a>
      </div>                          
    </div>
  </div>
</div>
                    </div>
                </div>

           <div id="banner" class="banner--section pd--80-0">
            <!-- Banner Slider Start -->
            <div class="banner--slider owl-carousel" data-owl-dots="true">
               
            </div>
  
    </div>

               
            </div>
        </div>
        <!-- testominial -->
        <div class="container-fluid" style="background-image: url('public/assets/img/gallery-img/gallery-item-01.jpg');">
    <div class="row">
        <div class="col-sm-12">
        <h3><strong>Client Feedback</strong></h3>
        <div class="seprator"></div>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >
              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item active">
                  <div class="row" style="padding: 20px">
                    
                    <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="col-sm-4">
                        <img src="http://demos1.showcasedemos.in/jntuicem2017/html/v1/assets/images/jack.jpg" class="img-responsive" >
                     
                       </div>
                       <div class="col-sm-8">
                        <h4><strong>Jack Andreson</strong></h4>
                        <p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                        <span>Officeal All Star Cafe</span>
                        <p class="testimonial_para">Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo en.</p>
                        </p>
                    </div>
                </div>
                    </div>
                  </div>
                </div>
               <div class="item">
                   <div class="row" style="padding: 20px">
                    
                    <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="col-sm-4">
                        <img src="http://demos1.showcasedemos.in/jntuicem2017/html/v1/assets/images/kiara.jpg" class="img-responsive" >
                    </div>
                        <div class="col-sm-8">
                        <h4><strong>Kiara Andreson</strong></h4>
                        <p class="testimonial_subtitle"><span>Chlinical Chemistry Technologist</span><br>
                        <span>Officeal All Star Cafe</span>
                        Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der Standard Demo-Text "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo en
                        </p>
                    </div>
                </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-10 col-sm-offset-1 controls testimonial_control " style="margin-top: -171px;">
                <a class="left fa fa-chevron-left btn btn-default testimonial_btn pull-left" href="#carousel-example-generic"
                  data-slide="prev"></a>

                <a class="right fa fa-chevron-right btn btn-default testimonial_btn pull-right" href="#carousel-example-generic"
                  data-slide="next"></a>
              </div>
        </div>
    </div>
</div>
        <!-- BHow can you g Section End -->

        <!-- Call To Action Section Start -->
       
        <!-- Call To Action Section End -->

@endsection


@push('sripts')
<script type="text/javascript">
    $(document).ready(function() {
  $('#media').carousel({
    pause: true,
    interval: false,
  });
});
</script>
@endpush