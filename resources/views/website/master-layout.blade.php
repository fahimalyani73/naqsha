<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- ==== Document Title ==== -->
    <title>Multipurpose </title>
    
    <!-- ==== Document Meta ==== -->
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- ==== Favicons ==== -->
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <link rel="icon" href="favicon.png" type="image/x-icon">

    <!-- ==== Google Font ==== -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700%7CSource+Sans+Pro:300,400,600" rel="stylesheet">

    <!-- ==== Font Awesome ==== -->
    <link href="{{asset('public/assets')}}/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- ==== jQuery UI ==== -->
    <link href="{{asset('public/assets')}}/css/jquery-ui.min.css" rel="stylesheet">
    
    <!-- ==== Bootstrap ==== -->
    <link href="{{asset('public/assets')}}/css/bootstrap.min.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
    
    <!-- ==== Owl Carousel Plugin ==== -->
    <link href="{{asset('public/assets')}}/css/owl.carousel.min.css" rel="stylesheet">
    
    <!-- ==== jQuery FakeLoader Plugin ==== -->
    <link href="{{asset('public/assets')}}/css/fakeLoader.min.css" rel="stylesheet">
    
    <!-- ==== jQuery Timepicker Plugin ==== -->
    <link href="{{asset('public/assets')}}/css/jquery.timepicker.min.css" rel="stylesheet">
    
    <!-- ==== jQuery Magnific Popup Plugin ==== -->
    <link href="{{asset('public/assets')}}/css/magnific-popup.css" rel="stylesheet">
    
    <!-- ==== Main Stylesheet ==== -->
    <link href="{{asset('public/assets')}}/css/style.css" rel="stylesheet">
    
    <!-- ==== Responsive Stylesheet ==== -->
    <link href="{{('public/assets')}}/css/responsive-style.css" rel="stylesheet">
    
    <!-- ==== Theme Color Stylesheet ==== -->
    <link href="{{asset('public/assets')}}/css/colors/color-1.css" rel="stylesheet" id="changeColorScheme">
    
    <!-- ==== Custom Stylesheet ==== -->
    <link href="{{asset('public/assets')}}/css/custom.css" rel="stylesheet">


</head>
@stack('css')
    <style>

    </style>
<body>
    
    <!-- Preloader Start -->
    <div class="preloader bg--color-theme"></div>
    <!-- Preloader End -->
    
    <!-- Wrapper Start -->
    <div class="wrapper" >
        <!-- Header Section Start -->
        <header class="header--section background-image-header" >
            <!-- Header Topbar Start -->
            <div class="header--topbar bg--color-dark">
                <div class="container">
                    <!-- Header Topbar Links Start -->
                    <ul class="nav links float--left hidden-xxs">
                         <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                    </ul>
                    <!-- Header Topbar Links End -->

                    <!-- Header Topbar Cart Start -->
                    
                    <!-- Header Topbar Cart End -->

                    <!-- Header Topbar Social Start -->
                    <ul class="nav social float--right">
                        <li><a href="#"> You want to register us Call us</a></li>
                        
                    </ul>
                    <!-- Header Topbar Social End -->
                </div>
            </div>
            <!-- Header Topbar End -->
@include('website.navigation')
@include('website.includes.home_page_slider')

            <!-- Header Navbar Top Start -->
            <div class="header--navbar-top">
                <div class="container">
                    <!-- Logo Start -->
                    <div class="logo float--left">
                        <div class="vc--parent">
                
                              <div style="padding-right: 1px;" id="headerNav" class="navbar-collapse collapse float--left">
                        <!-- Header Nav Links Start -->
                       
                        <!-- Header Nav Links End -->
                    </div>
                        </div>

                    </div>
                    <!-- Logo End -->

                   
                </div>


            </div>

            <!-- Header Navbar Top End -->

            <!-- Header Navbar Start -->
            <nav class="header--navbar navbar" data-trigger="sticky">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#headerNav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                   
                    
                    <!-- Header Navbar Search End -->
                </div>
            </nav>
            <!-- Header Navbar End -->
            @if(Request::is('constructions'))
                @include('website.filter')
            @endif
             @if(Request::is('interiors'))
                @include('website.filter')
            @endif
             @if(Request::is('townplaning'))
                @include('website.filter')
            @endif
            <br><br>
        </header>
        <!-- Header Section End -->
        @yield('content')

        <!-- Footer Section Start -->
        <div class="footer--section bg--color-dark">
            <div class="footer--copyright-border"></div>

            <div class="container bg--overlay">
                <div class="row reset--gutter">
                    <div class="col-md-3 bg--color-theme bg--overlay">
                        <!-- Footer About Start -->
                        <div class="footer--about">
                          

                            <div class="content">
                                <h4>About Us</h4>
                                <p>Naqsha has revolutionized the construction sector of Pakistan by providing online real-time quotations in just three simple steps.</p>
                            </div>

                            

                            
                        </div>
                        <!-- Footer About End -->
                    </div>

                    <div class="col-md-8">
                        <!-- Footer Widgets Start -->
                        <div class="footer--widgets row">
                            <!-- Footer Widget Start -->
                            <div class="footer--widget col-md-4">
                                <div class="widget--title">
                                    <h2 class="h4">Quick Links</h2>
                                </div>

                                <!-- Links Wdiget Start -->
                                <div class="links--widget">
                                    <ul class="nav">
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Home</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>About Us</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Terms and Conditions</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Faqs</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Blog</a></li>
                                        
                                    </ul>
                                </div>
                                <!-- Links Wdiget End -->
                            </div>
                            <!-- Footer Widget End -->

                            <!-- Footer Widget Start -->
                             <div class="footer--widget col-md-8">
                                <div class="links--widget">
                                    <ul class="nav">
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Architecture</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Construction</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Interior</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Landscape</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Town Planning</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i>Engineering</a></li>
                                        
                                    </ul>
                                </div>

                                
                            </div> 
                            <!-- Footer Widget End -->

                            <!-- Footer Widget Start -->
                            <div class="footer--widget col-md-4">
                                <div class="widget--title">
                                 
                                </div>

                                <!-- Subscribe Widget Start -->
                                <div class="subscribe--widget" data-form-validation="true">
                                    

                                        <div class="social">
                                        <h3 class="h6">Find Us On</h3>

                                        <ul class="nav">
                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Subscribe Widget End -->
                            </div>
                            <!-- Footer Widget End -->
                        </div>
                        <!-- Footer Widgets End -->

                        <!-- Footer Copyright Start -->
                        <div class="footer--copyright font--secondary clearfix">
                            <p class="float--left">&copy; Copyright 2017 | All Rights Reserved</p>
                            <p class="float--right"><a href="#">Naqsha</a> by Ihsan Web Solution</p>
                        </div>
                        <!-- Footer Copyright End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Section End -->

        <!-- Back To Top Button Start -->
        <div class="back-to-top-btn">
            <a href="#" class="btn btn-default active"><i class="fa fa-angle-up"></i></a>
        </div>
        <!-- Back To Top Button End -->
    </div>
    <!-- Wrapper End -->

    <!-- ==== jQuery Library ==== -->
    <!-- jQuery library -->
   
    <script src="{{asset('public/assets')}}/js/jquery.min.js"></script>

    <!-- ==== jQuery UI Library ==== -->

<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js">
</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

    <!-- ==== Bootstrap ==== -->
    <script src="{{asset('public/assets')}}/js/bootstrap.min.js"></script>

    <!-- ==== Owl Carousel Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/owl.carousel.min.js"></script>

    <!-- ==== Isotope Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/isotope.min.js"></script>

    <!-- ==== jQuery FakeLoader Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/fakeLoader.min.js"></script>

    <!-- ==== jQuery Sticky Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/jquery.sticky.min.js"></script>

    <!-- ==== jQuery Timepicker Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/jquery.timepicker.min.js"></script>

    <!-- ==== jQuery Magnific Popup Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/jquery.magnific-popup.min.js"></script>

    <!-- ==== jQuery Directional Hover Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/jquery.directional-hover.min.js"></script>

    <!-- ==== jQuery Validation Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/jquery.validate.min.js"></script>

    <!-- ==== jQuery Form Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/jquery.form.min.js"></script>

    <!-- ==== jQuery Waypoints Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/jquery.waypoints.min.js"></script>

    <!-- ==== jQuery CounterUp Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/jquery.counterup.min.js"></script>

    <!-- ==== RetinaJS Plugin ==== -->
    <script src="{{asset('public/assets')}}/js/retina.min.js"></script>

    <!-- ==== Main JavaScript ==== -->
    <script src="{{asset('public/assets')}}/js/main.js"></script>
@stack('scripts')
</body>
</html>




