<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>
    Argon Dashboard - Free Dashboard for Bootstrap 4 by Creative Tim
  </title>
  
  <!-- Favicon -->
  <link href="{{asset('public/admin_assets')}}/img/brand/favicon.png" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="{{asset('public/admin_assets')}}/https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{asset('public/admin_assets')}}/js/plugins/nucleo/css/nucleo.css" rel="stylesheet" />
  <link href="{{asset('public/admin_assets')}}/js/plugins/%40fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="{{asset('public/admin_assets')}}/css/argon-dashboard.min3f71.css?v=1.1.1" rel="stylesheet" />
  @stack('css')
</head>

<body class="">
