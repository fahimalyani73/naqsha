@extends('admin.adminmaster_layout')

@push('css')
@endpush
@section('content')
	<div class="row">
        <div class="col-md-12 mx-auto">
            <div class="card border-secondary">
                <div class="card-header">
                    <h3 class="mb-0 my-2">Add Construction </h3>
 
                </div>

                <div class="card-body">
                	@if(isset($tp))
                		<form class="form" role="form" autocomplete="off" method="post" action="{{ url('update-construction'.'/'.$tp->id) }}">
                	@else
				    <form class="form" role="form" autocomplete="off" method="post" action="{{ url('store-construction') }}">
				    @endif
				      @csrf
				      <div class="row">
				      <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="subcribed_company" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->subcribed_company) value="{{ $tp->subcribed_company }}" @endisset>
				          </div>
				      </div>
				      <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="plot_location" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->plot_location) value="{{ $tp->plot_location }}" @endisset>
				          </div>
				      </div>
				      <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="covered_area" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->covered_area) value="{{ $tp->covered_area }}" @endisset>
				          </div>
				      </div>
				      <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="rate_sqft" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->rate_sqft) value="{{ $tp->rate_sqft }}" @endisset>
				          </div>
				      </div>
				      <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="total" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->total) value="{{ $tp->total }}" @endisset>
				          </div>
				      </div>
				      <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="details" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->details) value="{{ $tp->details }}" @endisset>
				          </div>
				      </div>
				      <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="get_quotation" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->get_quotation) value="{{ $tp->get_quotation }}" @endisset>
				          </div>
				      </div>
				      <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="package" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->package) value="{{ $tp->package }}" @endisset>
				          </div>
				      </div>
				      <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="plot_size" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->plot_size) value="{{ $tp->plot_size }}" @endisset>
				          </div>
				      </div>
				          <div class="col-sm-6">
				          <div class="form-group">
				              <label for="state">State Name</label>
				              <input type="text" name="type" class="form-control" id="state" placeholder="Please Enter State name" @isset($tp->type) value="{{ $tp->type }}" @endisset>
				          </div>
				      </div>
				      
				        <div class="col-sm-12">
				          <div class="form-group">
				              <button type="submit" class="btn btn-success btn-lg float-right">
				              @if(isset($tp)) Update @else Create @endif</button>
				          </div>
				      </div>
				  </div>
				    </form>
				</div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush