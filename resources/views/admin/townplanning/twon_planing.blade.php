@extends('admin.adminmaster_layout')

@push('css')
<style type="text/css">
	@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css);
@import url(https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.4.3/css/mdb.min.css);

.hm-gradient {
    background-image: linear-gradient(to top, #f3e7e9 0%, #e3eeff 99%, #e3eeff 100%);
}
.darken-grey-text {
    color: #2E2E2E;
}
.input-group.md-form.form-sm.form-2 input {
    border: 1px solid #bdbdbd;
    border-top-left-radius: 0.25rem;
    border-bottom-left-radius: 0.25rem;
}
.input-group.md-form.form-sm.form-2 input.purple-border {
    border: 1px solid #9e9e9e;
}
.input-group.md-form.form-sm.form-2 input[type=text]:focus:not([readonly]).purple-border {
    border: 1px solid #ba68c8;
    box-shadow: none;
}
.form-2 .input-group-addon {
    border: 1px solid #ba68c8;
}
.danger-text {
    color: #ff3547; 
}  
.success-text {
    color: #00C851; 
}
.table-bordered.red-border, .table-bordered.red-border th, .table-bordered.red-border td {
    border: 1px solid #ff3547!important;
}        
.table.table-bordered th {
    text-align: center;
}
</style>
@endpush
@section('content')
 
    <main>
        
        <!--MDB Tables-->
        <div class="container mt-4">

            <div class="card mb-4">
                <div class="card-body">
                    <!-- Grid row -->
                    <div class="row">
                        <!-- Grid column -->
                        <div class="col-md-12">
                        	<a href="{{ url('add-town-planing') }}" class="btn btn-primary pull-right">Add Town Planning </a>
                        </div>
                        <div class="col-md-12">
                            <h2 class="pt-3 pb-4 text-center font-bold font-up danger-text">Town Planning</h2>
                        </div>
                        <!-- Grid column -->
                    </div>
                    <!-- Grid row -->
                    <table class="table table-bordered red-border text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Table heading</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tp as $key => $value)
                            <tr>
                                <th scope="row">{{ ++$key }}</th>
                                <td>{{ $value->subcribed_company }}</td>
                                <td>{{ $value->town_location }}</td>
                                <td>{{ $value->covered_area }}</td>
                                <td>{{ $value->rate_sqft }}</td>
                                <td>{{ $value->total }}</td>
                                <td>{{ $value->details }}</td>
                                <td>
                                    <a href="{{ url('edit-town-planing'.'/'.$value->id) }}" class="btn btn-primary">Update</a>
                                    <form method="post" action="{{ url('tp-destroy'.'/'.$value->id) }}">
                                        @csrf
                                    <input type="submit" class="btn btn-danger" value="Delete">
                                    </form>
                                </td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
          

        </div>
        <!--MDB Tables-->
      
    </main>
@endsection

@push('js')
@endpush